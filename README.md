# Hey you !

Si tu cherchais les cours que j'ai tapé c'est bien ici ! Si tu as bien pris les cours et que tu es là pour le vérifier mais que tu te rends comptes qu'il manque des petits trucs c'est sans doute le cas, *flemme oblige*, il s'agit souvent d'exemples. Pour tous les autres, *je vous vois*, ne venez pas râler, déjà que si vous êtes là c'est que vous n'avez pas pris le cours et que j'ai fais le boulot à votre place !

Tu trouveras aussi les cours en pdf mis à disposition par les profs sur Moodle. C'est tout à fait possible qu'il en manque certains.

___

Bref voici qui devrait t'aider un peu :

- les *.md* c'est les sources (normalement t'es pas con et tu connais)
- les *.pdf* bah c'est les pdf (si la tu connais toujours pas bah désolé mais j'peux rien faire pour toi)


- Titre minuscule = cours non terminé et/ou pas encore au propre
- Titre majuscule = cours terminé et au propre tout comme il faut

___

PS 1 : *Ne pas prendre les cours c'est mal*  
PS 2 : *Et j'te préviens, le bureau des plaintes n'est même pas ouvert pour les fotes d'ortograf*  
PS 3 : *Aller j'me prépare, faut pas que je rate le suivant*  
PS 4 : *Parce que 4*  
PS 5 : *T'aurais pas perdu au jeu toi ?*  
PS 6 : *Comment ça non ? T'as forcément lu les règles. Hein !? Non !? Bah les voila :*  
PS 6.1 : *Tout le monde participe au jeu*  
PS 6.2 : *Quand on pense au jeu, on perd au jeu*   
PS 6.3 : *Lorsque l'on perd au jeu, on doit le dire aux personnes avec qui nous nous trouvons*  
PS 6.4 : *Si une personne ne connaît pas le jeu, on doit lui expliquer les règles*  
PS 7 : *C'est bon t'as bien tout lu ?*  
PS 8 : *T'as plus aucune excuse maintenant (dit la personne qui passe son temps à tricher)*  
PS 9 : *Sinon tu connais Dreamcatcher ?*  
PS 10 : *Non ? C'est vachement bien pourtant*  
PS 11 : *Askip c'est validé par les vaches du Cantal en plus*  
PS 12 : *Ou pas, oui, non, peut-être, j'en sais rien en fait*  
PS 13 : *Faudrait leur demander*  
PS 14 : *C'est marrant d'écrire des PS quand même*  
PS 15 : *Faut juste avoir de l'inspi pour écrire des coco-conneries*  
PS 16 : *Eh mais, j'viendrais pas de spoiler un truc là ?*  
PS 17 : *Après ça dépend de quand tu lis ça*  
PS 18 : *En fait pour toi c'est une référence ça se trouve*  
PS 19 : *Sauf si t'es pas un gentil et là ça me fait de la peine*  
PS 20 : *J'vais donc m'arrêter là*  
PS 21 : *Askip ce nombre serait premier mais j'y crois pas vraiment*  
PS 22 : *Finalement j'pouvais pas m'arrêter là avant le PS 21*  
PS 23 : *Oué non, 23 c'est encore plus premier*  
PS 24 : *Bon revenons à nos moutons*  
PS 25 : *#mouton²*   
PS 26 : *J'vais pas écrire un mot de plus si des pas gentils sont dans les parages*    
PS 27 : *Oupsi, j'ai continué à écrire*  
PS 28 : *C'est que c'est dur de s'arrêter une fois la machine en marche*  
PS 29 : *Et je parle pas de politique (ok je sors)*  
