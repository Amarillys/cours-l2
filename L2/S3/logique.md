# **_Cours : Logique_**

___

## _I. Logique de proposition_

**Définition :** \
Une proposition est un énoncé pour lequel on peut attribuer la vérité vraie ou fausse. Une proposition est représentée par une variable.

**Exemple :** \
x:3+4=5 \
x=1 => 3+4=r est vraie \
x=0 => 3+4=r est fausse
___

**Syntaxe :**
-   Soit V = {x1, ..., xn} un ensemble de variables
-   C = {0, 1} = {vrai, faux} les constantes
-   les connecteurs {^, v, ¬, ->, <->}
-   des parenthèses ()
___

**Définition inductive :** \
Une formule est une suite de symboles construite à partir de l'alphabet avec les règles suivantes :
1. toutes proposition est une formule
2. si F est une formule alors ¬F est une formule
3. si F1 et F2 sont des formules alors (F1^F2), (F1vF2), (F1->F2), (F1<->F2) sont des formules
4. toute formule formule est obtenue à prtir de 1 par application des règles 2 et 3

**Exemple :** \
 V = {P, Q, r} \
F = ((P->Q)^(Q->r)v(Q^r))
___

**Sémantique :** \
Une interprétation est une application de V vers {0, 1} (associe une vérité à chaque variable).
___

**Calcule de la vérité d'une formule :** \
Soit F une formule.
1. si F=x une variable alors I(F)=I(x)
2. si F=¬F' alors I(F)=¬I(F')
3. si F=(F1^F2) alors I(F)=I(F1^F2)
___

**Définition :** \
Soit F une formule. F est dite :
- consistante s'il existe une interprétation I telle que I(F)=1
- valide ou tautologique si pour toute interprétation I, on a I(F)=1
- inconsistante s'il n'existe pas une interprétation I telle que I(F)=1 ou ¬F est une tautologie.

**Exemple :** \
V = {a, b, c} \
F1=(a->b) est consistante, non valide \
F2=(av¬a) est une tautologie \
F3=(a^¬a) est inconsistante
___

**Raisonnement :** \
Soient {F1, ..., Fn} un ensemble de formules. On dit qu'une formule F est une conséquence logique de {F1, ..., Fn}, notée {F1, ..., Fn}ㅑF, si pour toute interprétation I, si I(F1)=...=I(Fn)=1 alors I(F)=1.
___

**Théorème :** *modus ponens*\
{a, a->b}ㅑb

*Preuve :*

|  a  |  b  | a->b |
| :-: | :-: | :--: |
|  0  |  0  |   1  |
|  0  |  1  |   1  |
|  1  |  0  |   0  |
|  1  |  1  |   1  |

**Exemple :** \
{a->b, a->c}ㅑb->c?

|  a  |  b  |  c  | a->b | a->c | b->c |
| :-: | :-: | :-: | :--: | :--: | :--: |
|  0  |  0  |  0  |   1  |   1  |   1  |
|  0  |  0  |  1  |   1  |   1  |   1  |
|  0  |  1  |  0  |   1  |   1  |   0  |
|  0  |  1  |  1  |   1  |   1  |   1  |
|  1  |  0  |  0  |   0  |   0  |   1  |
|  1  |  0  |  1  |   0  |   1  |   1  |
|  1  |  1  |  0  |   1  |   0  |   0  |
|  1  |  1  |  1  |   1  |   1  |   1  |

4e ligne -> contre exemple

___

**Théorème :** \
{F1, ..., Fn}ㅑF <=> F1^...^Fn->F <=> (F1^...^Fn)^¬F est inconsistante

___

**Définition :** \
Deux formules F et F' sont équivalentes, notées (F<->F'), si pour toute interprétation I, on a I(F)=I(F'). \
Soit F une formule et P et Q deux variables. Une substitution, notée F(P/Q), est de remplacer la variable P par Q.
- F=P alors F(P/Q)=Q
- F=r et r≠P alors F(P/Q)=r
- si F=¬H alors F(P/Q)=¬H(P/Q)
- si F=F1vF2 alors F(P/Q)=F1(P/Q)vF2(P/Q)
___

**Raisonnement :**
- Etant donné un ensemble de connaissances, que peut-on déduire comme nouvelle connaissance ?
- Le principe du raisonnement doit être valide et complet (on n'oublie ucune conaissance)

**Formellement :** \
Soit ∆ = {F1, ..., Fn} un ensemble de formules et F une formule. On dit que F est une conséquence logique de ∆, notée ∆ㅑF, si quelque soit I appartenant {0, 1}^n si(I(F1)=1) et ... et (I(Fn)=1) alors I(F)=1.

**Exemple :** \
∆ = {P, (P->Q)} \
F = Q

|  P  |  Q  | P->Q |
| :-: | :-: | :--: |
|  0  |  0  |   1  |
|  0  |  1  |   1  |
|  1  |  0  |   0  |
|  1  |  1  |   1  |

∆ㅑQ
___

**Méthode sémantique :**
- Table de vérité : le nombre d'interprétations est exponentiel
- Arbre de décision : la taille de l'arbre est exponentielle
___

**Méthode syntaxique :** \
Arbre syntaxique : c'est une méthode indirecte. \
∆ = {F1, ..., Fn}ㅑF est équivalent à (F1^...^Fn)->F est valide \
<=> ¬((F1^...^Fn)->F) est inconsistante \
<=> (F1^...^Fn)^¬F est inconsistante
___

**Stratégie de preuve :** \
(F1^...^Fn)^¬F est inconsistante \
Arbre :
- Chaque noeud est une conjonction de formules
- Un noeud contenant 2 formules contradictoire (Fi et ¬Fi) est fermé
- Un noeud ne contenant que des formules atomiques ou leur négation sans formule contradictoire est dit ouvert (contre exemple)
___

**Règles de décomposition :**
- Règle \alpha :

|  \alpha   | \alpha 1  | \alpha 2  |
| :-------: | :-------: | :-------: |
|  (A1^A2)  |     A1    |     A2    |
|  ¬(A1VA2) |    ¬A1    |    ¬A2    |
| ¬(A1->A2) |     A1    |    ¬A2    |

- Règle \beta :

|   \beta   |  \beta 1  |  \beta 2  |
| :-------: | :-------: | :-------: |
|  (A1VA2)  |     A1    |     A2    |
|  ¬(A1^A2) |    ¬A1    |    ¬A2    |
| ¬(A1->A2) |    ¬A1    |     A2    |

___

**Algorithme de construction de l'arbre :**
- Initialisation : les étiquettes de la racine U(r)={F1, ..., Fn, ¬F}
- Etape inductive : on sélectionne une feuille non fermée l et soit U(l) une étiquette
  - si U(l) contient une paire contradictoire (Fi, ¬Fi) alors marquer l fermée
  - si U(l) ne contient que des littéraux alors marquer l ouvert
  - on sélectionne une formule non atomique A :
    - si A est une \alpha-formule alors créer un noeud l' descendant de l avec U(l')=(U(l)|{A})U{\alpha 1, \alpha 2}
    - si A est une \beta-formule, créer 2 noeuds l' et l'' avec U(l')={U(l)|{A}}U{B1} et U(l'')={U(l)|{A}}U{B2}
- Terminaison : toutes feuilles sont marquées ouvertes ou fermées

___

**Exemple :** \
∆ = {P, P->Q}ㅑQ ? \
(P^(P->Q))->Q \
<=> ¬(P^(P->Q))->Q) est inconsitante \
<=> P^(P->Q)^¬Q est inconsistante
___

La construction est non deterministe (arbre non unique) \
=> la taille de l'arbre peut etre exponentielle \
[www.umsu.de/logik/trees]
___

**Forme clausale d'une formule :**
- Forme normale conjonctive (CNF)
- Forme normale disjonctive (DNF)
___

**Définition :**
- Un littéral est soit une variable ou sa négation
- Une clause est une disjonction des littéraux
- F est dite sous forme clausal si F est une conjonction de clauses
- Toute formule admet une représentation sous forme clausal
___

**Forme clausale de Horn :** \
Une clause c est dite une clause de Horn si elle contient un unique littéral positif. Une clause de Horn est aussi une implication.
___

**Chainage avant :** \
Le chainage avant applique le modus ponens. Si toutes les premières sont vraies alors on rajoute la conclusion dans la base de faits.
- le chainage avant est guidé par les connaissances deja acquises
- le chainage avant s'arrete s'il n'y a plus de nouvelles connaissances (point fixe)

**Exemple :** \
∆ = {(r->s), ((f^g)->r), (s->f), (g^s)} un ensemble de connaissances \
BF : f, g, r, s \
L'inconvénient du chainage avant est la production de connaissances non nécessaires à la requete posée. La taille de la base de fait peut etre grande.
___

**Chainage arrière :** \
Il est guidé par le but (la requete). Un but recherché est *s*. Le chainage arrière considère toutes les règles ayant *s* en conclusion.
___

**Logique des prédicats :**
- logique du 1er ordre
- prédicat -> relation
- plus expressive que la logique proportionelle
- augmente la complexité du raisonnement

1. syntaxe :
  - les connecteurs : ¬, ^, v, ->, <-
  - les parenthèses : (, )
  - V: quantificateur universel, E: quantificateur existentiel
  - un ensemble V de variables

2. signature : \
  Z = (C, F, P) d'un language de prédicats est :
  - C : un ensemble de syntaxes constantes : a, b, c, ...
  - F : un ensemble de fonctions d'arité fixées : f, g, ...
  - P : un ensemble de relations d'arité fixées : P, Q, ...
  On suppose que C, F, P sont disjoints. \
  Une formule de la logique de prédicats est un mot

3. termes : \
  Soit \Sigma = (C, F, P) un ensemble de signatures. \
  T : l'ensemble des termes est définit inductivement.
  - toute constante est un terme : C C_ T
  - toute variable est un terme : V C_ T
  - si f \appartient F d'arité n et t1, ..., tn sont des éléments alors f(t1, ..., tn) \appartient T
  - un terme est dit fermé s'il ne contient pas de variable

**Exemple :** \
Z = (C = {1, 2}, F = {f, g}, P = {P, Q}) \
V = {x, y}
- 1, 2, x, y sont des termes
- f(x), f(1), f(f(1)), f(f(f(x))), g(1, 2)

___

**Formules :**

1. **Formule atomique :** \
 Une formule atomique est oblème par la règle : Si P est un symbole de prédicats d'arité n et t1, ..., tn sont des termes alors P(t1, ..., tn) est une formule atomique. \
 L'ensemble des formules est définit inductivement par :
 1. toute formule atomique est une formule
 2. si F est une formule alors ¬F est une formule
 3. si F et G sont des formules alors (F^G), (FvG), (F->G), (F<->G) sont des formules
 4. si F est une formule et x une variable alors VxF, ExF sont des formules
 On dit que G est une sous formule de F si G apparait dans un noeud de l'arbre de décomposition.
___

**Variables libres et liées :**
- une occurence d'une variable x dans F est dite liée si elle apparait dans un dans une sous formule de F qui commence par Vx ou Ex
- une variable qui n'est pas liée est dite libre
- une formule est dite fermée si elle ne contient pas de variables libres. On peut tojours renommer chaque variable soit libre ou liée
- substitution : soit F une formule , x une variable et t un terme. F(x/t) est le remplacement de chaque occurence libre de x par t.
___

**Définition :** \
Soit \Sigma = (C, F, P) une signature, une réalisation M sur \Sigma est :
- un ensemble ∆ non vide appelé domaine
- un élément Cm de ∆, pour toute constante k \appartient C
- une application fm de ∆m->∆ pour chaque fonction f \appartient F d'arité n
- un sous-ensemble Pm de ∆m pour chaque symbole de prédicat d'arité n

___

**Interprétation :** \
Soit M une valuation I pour m est une fonction de V-> ∆  
Remarque : Elle sert pour les variables libres  
Soit t un terme de la forme t(x1, ..., xn) et x1, ..., xn sont des variables libres. L'interprétation t^M du terme t pour la valuation I, noté t^M[I], est définit inductivement par :
1. toute variable est interprété par sa valuation  
  si t = xi \appartient V alors t^M = I(xi)
2. si t = c \appartient C alors t^M = c^M
3. si t = f(t1, ..., tn) alors t^M = f^M(t1^M, ..., tn^M)

___

**Interprétation de formule atomique :** \
Soit F = P(t1, ..., tn) avec {x1, ..., xn} les variables libres et I une valuation, on dit que la valuation I satisfait la formule P(t1, ..., tn) si (t1^M[I], ..., tn^M[I]) \appartient P^M, noté Mㅑ_IF

___

**Interprétation d'une formule :** \
Soit M une réalisation, I une valuation et f une formule sur les variables x1, ..., xn
1. F est atomique
2. si F = ¬G alors MㅑF_IF <=> Mㅑ(barré)_IG
3. si F = GVH alors Mㅑ_IF <=> Mㅑ_IG ou Mㅑ_IH
4. si F = G^H alors Mㅑ_IF <=> Mㅑ_IG et Mㅑ_IH
5. si F = G->H alors Mㅑ_IF <=> Mㅑ(barré)_IG ou Mㅑ_IH
6. si F = AxG(x1, ..., xn) alors Mㅑ_IF <=> Aa \appartient ∆ Mㅑ_IG avec I(xi)= I'(xi) et I'(x) = a
7. si F = ExG(x1, ..., xn) alors Mㅑ_IF <=> Ea \appartient ∆ Mㅑ_IG avec I(xi)= I'(xi) et I'(x) = a
___

Soit \SIGMA une signature et M une réalisation et une valuation I : V -> D des variables libres. \
Si une formule F est vraie pour M et I, on dit que M satisfait F pour I, notée Mㅑ_I F.

___

**Définitions :**
- Si F est une formule fermée (sans variable libre), M est dit un modèle pour f, noté MㅑF.
- Soit F une formule, une réalisation M est un modèle de f si pour toute valuation I de variables libres Mㅑ_I F, notée MㅑF.
- Une formule F est dite consistante s'il existe une réalisation M telle que Mㅑf.
- Une formule F est une tautologie ou valide si pour toute réalisation M, MㅑF.

**Exemple :** \
\SIGMA = (C = ø, F = ø, P ={R^M,P^M}}) \
D = {a, b, c} \
R^M = {(a, b), (a, c), (b, a), (c, c)} \
P^M = {a, c} \
F = (Ax Ey R(x, y)->P(z)) \
La seule variable libre est z \
On a 3 cas :
- I(z) = a :
  (Vx Ey R(x, y)-> P(a)) \
  Puisque P(a) est vraie alors F est vraie
- I(z) = b :
  (Vx Ey R(x, y)->P(b)) \
  P(b) est fausse, on distingue 3 cas :
  1. I'(x) = a : (Ey R(a, y)->P(b)) => I(y)=0 => I(Ey R(a, y)) est vraie => F est fausse
  2. I'(x) = b : F est fausse
  3. I'(x) = c : (Ey R(c, y)->P(b)) => I(y)=0 => I(Ey R(c, y)) est vraie => F est fausse
- I(z) = c :
  (Vx Ey R(x, y)-> P(c)) \
  P(c) est vraie donc F est vraie

Donc F n'est pas un modèle de F.  

___

**Equivalence :** \
Soit F une formule :
- ¬Vx F =_ Ex ¬F
- ¬Ex F =_ Vx ¬F
- Vx Vy F =_ Vy Vx F
- Ex Ey F =_ Ey Ex F
- Ex Vy F /=_ Vy Ex F

___

**Proposition :** \
Supposons que x n'est pas une variable libre dans C.
- VxG =_ ExG =_ G
- (Vx FVG) =_ Vx(FVG)
- 
