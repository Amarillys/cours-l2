# **Fichiers et stdio**
___

### ***I. Manipulation de fichiers***

Pour manipuler des fichiers, il existe un type *FILE*. Avant de commencer une opération, il faut l'ouvrir et créer un descripteur de fichier :
```c
FILE * fd = fopen("nom_fichier", "mode d'ouverture");
```
Les modes d'ouvertures sont :
- "r" (lecture seule)
- "w" (écriture seule, peut effacer le fichier)
- "a" (ouvre en écriture et place le curseur à la fin)
- "rb", "wb", "ab" (mode binaire)
- "r+" (lecture et écriture, pointeur de lecture au début)
- "w+" (lecture et écriture, crée le fichier s'il n'existe pas ou efface son contenu s'il existe)
- "a+" (lecture et écriture, pointeur de lecture au début et d'écriture à la fin, crée le fichier s'il n'existe pas)

Une fois l'utilisation du fichier terminé, pour libérer les ressources et provoquer les écritures non faites :
```c
fclose(fd);
```

La fonction *fopen()* peut :
- réussir (fd != NULL)
- échouer (fd == NULL)

L'échec peut aussi être du à plusieurs raisons :
- le fichier n'existe pas
- le fichier n'a pas les droits compatibles avec le programme
- le disque peut être plein

La fonction *fclose()* renvoit 0 si elle réussi sinon elle renvoit la valeur EOF (end of file) : soit le fichier est deja fermé, soit il y a un autre problème.

Si *fd == NULL*, on peut afficher l'erreur rencontrée avec *perror()* :
```c
If(fd == NULL) {
    perror("problème fichier : ");
}  
```   

***Remarque :*** un fichier peut être ouvert plusieurs fois en parallèle.

Pour chaque programme il existe 3 flux de fichiers (descripteurs) qui sont deja ouverts :
- stdin (0, entrée standard depuis le terminal)
- stdout (1, sortie standard dans le terminal)
- stderr (2, sortie d'erreur dans le terminal, peut etre utilisé pour les erreurs et le déboggage)

```bash
$ ls ./prog 2>fichier
```
___

### ***II. Fonctions d'écritures***

La librairie *stdio* fournit plusieurs fonctions pour faire une écriture formatée :

#### A. Fonction *printf()*

Les fonctions *printf()* et ses variantes sont bufferisées (tamponées). Toutes les opérations d'entrée/sortie sont des opérations très lentes (par rapport à la vitesse de calcul de la machine). Au lieu de provoquer les écritures lorsqu'elles sont demandées, le système attend "suffisament" de données pour déclencher l'écriture.

- avantage : améliore les performances
- inconvenient : certains affichages n'apparaissent pas

La fonction *flush()* et ses variantes permettent de forcer l'écriture.

#### B. Fonction *fprintf()*

La fonction *fprintf()* :
```c
int fprintf(FILE * strem, const char * format, ...)
```

Le format est le même que *printf()* et ce sont des fonctions. Le *stream* correspond au descripteur du fichier dans lequel on veut écrire.

*fprintf()* renvoit le nombre d'octets effectivement écrits sinon elle renvoit un nombre négatif (erreur d'écriture).

Une erreur peut etre du à :
- le disque est plein
- le fichier était ouvert en lecture seulement
- ...

Pour faire une écriture standard, *printf()* correspond à *fprintf(stdout, ...)* et *fprintf(stderr, ...)* pour faire une écriture sur la sortie erreur.

Il y a des situations où l'écriture formatée ne va pas correspondre aux besoins.

#### C. Fonction *fputc()*

La fonction *fputc()* écrit un caractère dans le flux. Elle renvoit EOF s'il y a un problème sinon elle renvoit un entier.
```c
int fputc(int char, FILE * stream)
```

#### D. Fonction *fputs()*

La fonction *fputs* renvoit le nombre de caractères écrits (sans '\0') ou EOF s'il y a un problème.
```c
int fputs(char * str, FILE * stream)
```

#### E. Fonction *fwrite()*

La fonction *fwrite()* permet de tout écrire.
```c
size_t fwrite(const void * ptr, size_t size, size_t nb_elements, FILE * stream);
```

Un pointeur de type void permet de pointer sur n'importe quelle zone en mémoire mais qui contient le même type.
```c
int * tab = malloc(100*sizeof(int));
int k = fwrite((void *)tab, sizeof(int), 100, fd);
```
Dans cet exemple, *fwrite()* écrit dans le fichier les 100 entiers où chaque entier est écrit comme les 4 octets de sa représentation en mémoire. La taille de la donnée codée sera possiblement plus petite qu'avec le format texte. On pourra donc gagner du temps à la lecture.
___

### ***III. Opérations de lecture et d'écriture***

#### A. Fonction *fscanf()*

La fonction *fscanf()* permet de faire des lectures formatées depuis un flux de fichiers.
```c
int fscanf(FILE * fd, const char * format, ...);
```
*fscanf()* renvoit le nombre d'affectations valides qu'il est parvenu à effectuer.

```c
int a, b, c;
fscanf(fd, "%d %d %d", &a, &b, &c);
```
Dans cet exemple, on veut lire 3 entiers (et faire la conversion) et placer le contenu dans les variables.

Pour un caractère :
- renvoit le caractère ou EOF
- avance dans le flux de lecture

#### B. Fonction *fgets()*

```c
char * fgets(char * str, int n, FILE * stream);
```
Cette fonction renvoit un pointeur sur la chaine sinon NULL s'il y a un problème. n est la taille du tableau '\0' compris. Au plus *n-1* caractères lus.

Ce qui arrête la lecture :
- on a lu *n-1* caractères depuis *stream*
- on a lu '\0'
- on a atteint la fin du fichier

#### C. Fonction *fgetc()*

```c
int fgetc(FILE * stream);
```
Cette fonction permet de lire un caractère :
- elle renvoit le caractère ou sizeof
- elle avance dans le flux de lecture

#### D. Fonction *fread()*

C'est une fonction analogue de *fwrite()* qui fait des lectures non formattées, très utile pour faire de la lecture de contenu binaire, faire de la copie de fichiers.

```c
size_t fread(void * ptr, size_t size, size_t nb_element, FILE * fd);
```

Cette fonction renvoit le nombre d'octets réellement lus.

```c
#define SIZE 512

char * buffer = malloc(sizeof(char)*SIZE);
FILE * fd = fopen(...);
int k;
while(k = fread(buffer, sizeof(char), SIZE, fd)) {
  // toute valeur ≠ 0 est considérée comme vraie
  printf("%d\n", k);
}
```
De plus, comme dans le flot, pour toute manipulation de fichier, on va faire les opérations (lecture et écriture) à partir d'un curseur.
___

### ***IV. Curseur***

Le curseur est une position dans le fichier. Par exemple en lecture, le curseur est placé au début du fichier (position 0). Le curseur est associé à un descripteur de fichier. Si on veut parcourir le fichier en parallèle, il faudra ouvrir plusieurs fois le fichier.

#### A. Fonction *fgetpos*

```c
int fgetpos(FILE * fd, fpos_t * pos);
```
La fonction renvoit 0 s'il n'y a pas de problème ou un entier ≠ 0 sinon. Elle renseigne la position du pointeur pour une utilisation avec la fonction *setpos()*.

#### B. Fonction *ftell()*

```c
long int ftell(FILE * fd);
```
Cette fonction n'a qu'un seul paramètre : le fichier à parcourir. Elle renvoit la position du pointeur ou -1L s'il y a une erreur. *ftell()* donne une position en nombre d'octets.

**Remarque :** *ftell()* et *fgetpos()* sont des accesseurs.

#### C. Fonction *fsetpos()*

```c
int fsetpos(FILE * fd, const fpos_t * pos);
```
Cette fonction déplace le curseur à la position pointée par *pos*. Elle renvoit 0 s'il n'y a pas de problème ou un entier ≠ 0 sinon. Un fichier peut être accédé par plusieurs programmes de manière concurrente :
- si tous les programmes sont en lecture : pas de problème
- si au moins un est en écriture : problèmes possibles

#### D. Fonction *fseek()*

La fonction *fseek()* permet de déplacer le curseur.
```c
int fseek(FILE * fd, long int offset, int whence);
```
*whence* est la position de départ du décalage et *offset* le décalage à partir de *whence*. *whence* a 3 valeurs possibles :
- SEEK_CUR (position courante)
- SEEK_SET (début du fichier)
- SEEK_END (fin du fichier)

```c
FILE * fd = fopen(...);
fseek(fd, 1000000, SEEK_SET);
// amène la taille du fichier à 1 000 000 d'octets
```

Pour récupérer la taille du fichier (sans faire d'appels système) :

```c
//on place le curseur à la fin
fseek(fd, 0, SEEK_END);
long int t = ftell(fd);
// t est la taille en octet du fichier

```

#### E. Fonctions diverses

- **Fonction *fflush()***
```c
int fflush(FILE * fd);
```
Cette fonction permet de forcer les écritures dans un fichier.

- **Fonction *fclose()***  
```c
int fclose(FILE * fd);
```
Elle permet de s'assurer que toutes les opérations en attentes ont été faites.

- **Fonction *feof()***  
```c
int feof(FILE * fd);
```
Elle renvoit un entier ≠ 0 si c'est la fin du fichier ou 0 sinon.
___

###  ***V. Manipulation de fichiers du stdio***

#### A. Supprimer/Renommer un fichier

```c
int remove(const char * nom_fichier);
```
Cette fonction supprime le fichier et renvoit 0 ou 1 s'il y a un problème.


```c
int rename(const char * ancien_nom, const char * nouveau_nom);
```
Cette fonction renomme un fichier et renvoit 0 ou -1 s'il y a une erreur.

#### B. Nom/Fichier temporaire

```c
char * tmpname(char *str);
```
Cette fonction génère un nom de fichier temporaire qui n'existe pas dans le répertoire. *str* est la zone où stocker la chaine pointée vers un buffer interne. La fonction renvoit *NULL* s'il y a une erreur.

```c
FILE * tmpfile(void);
```
Cette fonction crée un fichier temporaire (on peut faire des lectures et écritures). Le fichier est automatiquement supprimé à l'appel de *fclose();*.

#### C. Fonction *sscanf()* et *sprinf()*

Cette fonction fait le formatage des lectures et les conversions de types.
```c
int sscanf(char * str, const char * format, ...);
```
Comme un *scanf()* cette fontion renvoit le nombre d'affectations réussies.
```c
char * str = "123 456 789"
int a, b;
int k = sscanf(str, "%d %d", 2a, 2b);
// a et b valent 123 et 456 et k vaut 2
// si str est de la forme "nb text nb ...", k vaut 1 et seul a sera affecté
```
Le curseur n'avance pas dans la chaine donc on doit gérer autrement le découpage des chaines.

*sprinf()* est la même chose que *prinf()* ou *fprintf()* mais pour les chaines de caratères.


___

# Fonctions et unions
___

### ***I. Fonctions variadiques***

Une fonction variadique est une fonction qui a un nombre indéterminé de paramètres (*printf()*, *scanf()*). Le language C propose un mécanisme qui permet de déclarer et écrire ce genre de fonctions. Pour déclarer une fonction variadique, on va a voir recours à une ellipse '...' :
- l'ellipse est le dernier paramètre
- la fonction a au moins un autre paramètre

Le fonctionnement d'une telle fonction est risqué, il n'y a pas de vérification de type à la compilation et s'appuie essentiellement sur le pré-processeur.

```c
int max(int n, ...);
```

On va utiliser 3 commandes pré-processeur qui vont nous permettre de gérer la liste des variables :
```c
va_start (va_list, last);
// commencer le traitement, *last* correspond au dernier vrai paramètre
```

```c
va_arg (va_list, type);
// acceder aux paramètres, permet d'extraire un élément
```

```c
// pour extraire un entier :
int k = va_arg(ap, int);
// il faut connaitre le nombre d'argument qui a été passé :
// - soit on fait comme printf
// - soit on a explicitemenet un paramètre réel qui donne le nombre de valeur dans
// l'ellipse
```

```c
va_close (va_list ap);
// fermer le traitement
```

```c
int mdv(int n, ...) {
  int r, i, m = 0;
  va_list = ap;
  va_start(ap, n);
  for (i=0; i<n; i++) {
    r = va_arg(ap);
    if (r>m)
      m = r;
  }
  va_close(ap);
  return m;
}
```
___

### ***II. Fonctions utiles stdlib***


#### A. La fonction *exit()*

```c
exit(int k);
```
Elle permet de terminer le programme depuis n'importe quelle fonction. Le paramètre k donne une valeur de retour du programme. Dans le shell on a la variable *$?* qui est la valeur de retour du programme lancé.

```bash
$ ./prog
$ echo $?
```

A la fin d'un main, mettre *return 0;* permet de mettre cette variable à jour.
*exit()* fait une fermeture propre, il ferme tous les fichiers qui sont ouvert.

#### B. la fonction *system()*

```c
int system(const char * cmd);
```

Elle permet d'appeler une commande système et renvoit le code retour de la commande exécuté.

___

### ***III. Pointeurs de fonctions***

De la même manière, qu'on peut faire des pointeurs sur des variables , on va faire des pointeurs sur des fonctions. Les pointeurs de fonctions vont permettre d'introduire de la généralité.

```c
int (* fonction) (int, int);
// ici, la fonction pointée prend 2 paramètres entiers
```

Cette déclaration peut être faite à l'intérieur ou à l'extérieur d'une fonction.

```c
printf("max(%d, %d) = %d", 1, 2, max(1, 2));
// ou
fonction = &max;
printf("%d", fonction(1, 2));
```

Un pointeur de fonction n'est jamais une définition de fonction.

Par exemple pour un algorithme de tri, on va pouvoir modifier l'opérateur de comparaison. On peut avoir un pointeur sur l'opérateur.

La stdlib fournit une fonction *qsort()* très générique.

```c
void qsort(void *base, size_t nb_element, size_t size,
           int (*compar(void *, void *)));
```

Cette fonction *qsort()* peut trier n'importe quel tableau selon un ordre total pourvu qu'on fournisse un opérateur de comparaison.
___

### ***IV. Les unions***

Les unions ressemblent à des structures mais avec un comportement différent. L'union contrairement à la structure, met toutes les variables dans la même zone mémoire. La taille d'une union sera la taille du plus grand élément.

```c
union simple {
  int x;
  float y;
};

int main() {
  union simple A;
  A.x = 10;
  A.y = 25.5;
}
```

___

# **Librairies partagées**
___

### ***I. Role de l'éditeur de liens***

Par exemple, on a 2 fichiers : main.c et fonction.

```bash
$ gcc -c main.c
$ gcc -c fonction.c
```

on obtient les fichiers objets main.o et fonction.o qui contiennent du code
assemblés

L'édition de liens intervient pour connecter les fichiers objets et
obtenir un éxécutable. L'éditeur de lien va regrouper tout le nécessaire pour éxécuter le contenu de fonction.o et main.o. On obtient un éxécutable avec tout le code nécéessaire pour que ça marche.

```bash
$ gcc -o main *.o
```

L'inconvénient est que si on a des librairies qui sont très utilisées par
plusieurs programmes, on est obligé d'inclure tout le code objet à chaque
fois.
___


### ***II. Librairies partagées***

#### A. Concept

Le concept de librairie partagée consiste à séparer le code des fonctions fréquemment utilisées et les rendre accessibles au programmeur. \
Sur linux : fichier.so \
Sur windows : fichier.dll

Une librairie va être le regroupement de un ou plusieurs fichiers .o. Si on a un éxécutable X qui utilise une librairie f :

```bash
$ gcc -o X X.o -lf
```

Sur le système il y a des répertoires désignés où il faut chercher les librairies. On peut indiquer le chemin avec -L/chemin/vers/lib.
___

#### B. Créer une librairie partagée

Par exemple on a 2 fichiers : f1.c et f2.c qui vont constituer notre librairie f.

```bash
$ gcc -o libtest.so -shared -fPIC -wl, -soname, libtest.so -c f1.c f2.c
```

Pour l'éxécutable comme P interrogation des fonctions de la librarie est dynamique. Il faut garantir que le programme peut trouver la librairie. On peut modifier dynamiquement le chemin des liens vers les libraries.

```bash
$ export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/chemin/vers/lib
$ gcc -o X X.o -lf -wl -R/chemin/vers/lib
```
___

### ***III. Gestion des signaux***

Le système d'exploitation a plusieurs manières de communiquer avec les programmes. une façon simple de communiquer est l'envoie de signaux. Un signal est un message simplement constituer d'un code. Par défaut lorsqu'un programme reçoit un signal, il va effectuer l'action prévue. pour certains signaux, on peut mettre en place un système d'intersection du signal et déclencher des actions appropriées.
___

#### A. Liste des signaux

- SIGBUS  
  Problème d'adressage sur le bus de données (matériel)

- SIGSEGV  
  Erreur de segmentation (génère un one dump)

- SIGFPE  
  (FPE = Floating point exception)  
  Erreur de manipulation de nombres flottants (sur des entiers pour une division par 0)

- SIGHUP  
  Signal envoyé au programme lorsque le terminal qui l'a lancé a été fermé

- SIGINT  
  Interruption, correspond a un ctrl+C dans le terminal

- SIGKILL  
  Déclenche l'arret immédiat du programme

- SIGSTOP  
  Mise en pause du programme, correpond à un ctrl+Z

- SIGCONT  
  Reprendre l'éxécution

- SIGTERM  
  Demande la terminaison du programme de manière plus diplomatique que SIGKILL

- SIGUSR1, SIGUSR2  
  SIgnaux utilisateur

Depuis le terminal on peut envoyer un signal à un programme avec la commande ```$ kill pid```. Par défaut, le signal envoyé est SIGTERM (ou SIGINT). ```$ kill -9 pid``` renvoit SIGKILL.  
___

#### B. Mise en place du gestionnaire de signaux

*signal()* définie dans signal.h

```c
typedef void (*signalhandler)(int);
signalhandler signal(int, (*sighandler)(int));
```

Le premier paramètre est le numéro du signal et el sond le pointeur de fonction du gestionnaire. pour intercepter un signal particulier, il faut faire appel à signal dès le début du programme. Pour les numéros des signaux, il faut utiliser les macros données en majuscule (SIG*).
___

#### C. Autres instructions

- volatile
  ```c
  volatile int *;
  ```
  On indique au compilateur que cette variable pourra etre modifiée par une autre entitée, de ne pas faire d'optimisation sur la variable.

- const \
  Indique que la variable ne peut pas être modifiée.

- inline \
  Pour des fonctions. *inline* indique au compilateur qu'à chaque fois qu'on appelle la fonction, on remplace l'appel de fonction par le code de la fonction. Permet d'économiser sur un appel de fonction et un retour.

  ```c
  inline int max(int a, int b) {
    return a>b?a:b;
  }
  ```

- static \
  Généralement utilisé pour une variable. La valeur de la variable est conservée. C'est come une variable globale mais qui peut être déclarée à l'intérieur d'une fonction.


- extern \
  On souhaite utiliser une variable globale mais elle est définie dans un autre fichier .c
