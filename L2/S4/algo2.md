# Algorithme II

## Chapitre 1 : Algorithmes, valeurs, types et éléments de langage

**Définition :**    
 Un *algorithme* est une suite d'opérations pour résoudre un problème donné. Un problème prend en entrée des données et nous devons les représenter par une structure de données pour que notre algorithme les manipule. La mise en oeuvre d'un algorithme est l'implémentation dans un langage de programmation particulier.

**Théorème de l'indécidabilité :**  
Il existe des problèmes mathématiques pour lesquels il ne peut pas exister d'algorithme pour les résoudre. Il n'y a pas d'algorithme pour les identifier.

> **Exemple :**   
> Problème de l'arrêt d'un algorithme

**Théorème de la thèse de Church-Turing :**  
Les problèmes pouvant être résolus par un algorithme avec des hypothèses simples de physique sont exactement ceux que l'ont peut résoudre avec un ordinateur.

### I. Données

**Définitions :**   
Un *identifiant* est une suite de caractères alphanumériques commençant par une lettre.  
Un *type* est un ensemble de valeurs muni d'un ensemble d'opérations sur ces valeurs et d'un ensemble de propriétés vérifiées par les opérations. Un type sera représenté par un identifiant.  

> **Exemple :**  
> Les entiers relatifs est un type. L'addition, la soustraction, la multiplications et la division dont des opérations. LA division par 0 est indéfinie : c'est une propriété.  

Une *donnée* est soit une constante, soit une entité à laquelle on donne un type, une valeur et un identifiant.  
Une *constante* est une valeur d'un certain type.  
Une *variable* est un identifiant qui désigne un espace de stockage en mémoire et qui a un type. Dans cet espace de stockage on ne peut y stocker que des objets de ce type.  
Une *affectation* d'une valeur v à une variable x consiste à stocker dans l'espace de stockage de x la valeur v. Une variable ne peut être manipulée que si on lui a affectée une valeur. Une variable peut être manipulée comme n'importe quelle valeur de son type.

Il y a 2 familles de types :
- Les types primitifs : ceux pour lesquels les valeurs sont atomiques (on ne peut pas les décomposer par rapport au constructeur de type).

> **Exemples :**  
> Entier, réel, booléen, caractère, chaine de caractère

- Les types composés : ils sont construits à partie d'autres types. On utilise des constructeurs pour les définir.

> **Exemples :**  
> Tableau : on utilisera la meme syntaxe qu'en C.   
> Produit : si A, B sont des types et vA, vB leur valeur respectives alors A*B est un type avec comme valeurs l'ensemble {vA, vB}.  
> Somme : si A et B sont des types alors A+B est un type et ses valeurs sont l'ensemble composé de l'union des valeurs des types A et B.  
> Enregistrement : un type composé de plusieurs entités appelés champs, chacun ayant un type et un identifiant. C'est un type produit mais non ordre entre les différentes valeurs du tuples. Chaque champs est un espace de stockage en mémoire. Les valeurs sont toutes les façons de donner une valeur à chacun des champs. On utilisera la structure de struct en C.

### II. Pseudo-code

**Passage des paramètres des fonctions:**  
- Passage par valeurs : l'argument est copié et la copie est donnée à la fonction appelée. C'est le passage par défaut.
- Passage par référence : on copie l'adresse de l'argument et la copie est donnée à la fonction appelée. C'est le passage par défaut pour les tableaux.

**Propriétés des tableaux statiques :**  
Si on connait le type des valeurs et le nombre de valeurs, on alloue dans la mémoire x mots contigus en supposant que x = taille d'une valeur✱nombre de valeurs. Si le premier mot se trouve à l'adresse C, pour accéder à la i-ème valeur : C+(i-1)✱y.

|                 | Valeur | Référence |
| :-------------: | :----: | :-------: |
| Rapidité        | non    | oui       |
| Coût en mémoire | oui    | non       |
| Effet de bord   | non    | oui       |
| Sécurité        | oui    | non       |

**Récursivité :**  
Si on a un paramètre n, à chaque appel récursif ce paramètre n doit diminuer. Il faut une condition d’arrêt pour que les appels récursifs s’arrêtent. Une fonction récursive doit disposer d'assez de mémoire pour les appels (dans la pile d’exécution).

## Chapitre 2 : Types de données abstraits

### I. Définition

Les ensembles que l'on manipule dans nos algorithmes sont différents des ensembles mathématiques parce qu'on les modifie lors de l’exécution de l'algorithme. Les ensembles et opérations spécifiées par le cahier des charges sont appelés *types de données à modéliser* (TDM). La formalisation mathématique des TDM est ce que l'on appelle *types de données abstraits* (TDA). Les *types de données concrets* (TDC) sont des implémentations des TDA.

Un TDA est une entité constituée d'une signature et d'un ensemble d'axomes. La *signature* est composée d'un identifiant, les identifiants et signatures des opérations et enfin, les types pré-définis à utiliser. Les *axiomes* définissent le comportement des opérations.

**Questions fondamentales avec les TDA :**
- Complétude (tous les comportements sont modélisés ?)
- Consistance (les axiomes sont-ils contradictoires ?)

> **Exemples :**  
- On veut manipuler des vecteurs de réels. On définit le TDA vecteurReel qui utilise 2 types : réel et entier qui supportent les opérations suivantes :  
ObtenirI : VecteurReel ✱ Entier ⟶ Reel  
ModifierI :  VecteurReel ✱ Entier ✱ Reel ⟶ Reel  
Sup : VecteurReel ⟶ Entier  
Inf : VecteurReel ⟶ Entier  
- Axiomes : Inf(v) ≤ i ≤ Sup(v) ⇨ ObtenirI(Modifier(v, i, e), i) = e
- Implémentation possible : tableau de réels, le plus petit à la case 0, le plus grand à la case n-1.

### II. Dictionnaire

Un *dictionnaire* est un ensemble qui supporte les opérations suivantes : insertion, suppression et test d'appartenance.

Le TDA Dico_T, qui utilise les 2 types booléen et T, a comme opérations :  
- creerDico : () ⟶ Dico_T
- insererDico : Dico_T ✱ T ⟶ Dico_T
- supprimerDico : Dico_T ✱ T ⟶ Dico_T
- appartenanceDico : Dico_T ✱ T ⟶ booléen

Axiomes :
- appartenanceDico(creerDico(), x) = Faux ∀x∈T
- appartenanceDico(supprimerDico(D, x), x) = Faux (pas de doublons dans le dictionnaire)
- appartenanceDIco(insererDico(D, x), x) = Vrai

### III. Ensemble dynamique

C'est le TDA pour représenter un ordre total.

**Rappel :** ≤⊆VxV est un ordre total si réflexif, anti-symétrique, transistif et ∀x,y x≤y ou y≤x

Ens_Dyn utilise les types T et booléen et a comme opérations :
- creerEnsDyn : () ⟶ Ens_Dyn
- insererEnsDyn : Ens_Dyn ✱ T ⟶ Ens_Dyn
- appartenanceEns_Dyn : Ens_Dyn ✱ T ⟶ booléen
- minEns_Dyn : Ens_Dyn ⟶ T
- maxEns_Dyn : Ens_Dyn ⟶ T
- prédécesseurEns_Dyn : Ens_Dyn ✱ T ⟶ T
- successeurEns_Dyn : Ens_Dyn ✱ T ⟶ T

Axiomes : ceux des dictionnaires et le fait que minEns_Dyn retourne le plus petit, maxEns_Dyn le plus grand, ... Le minimum n'a pas de prédecesseur, le maximum n'a pas de sucesseur. La relation définie est un ordre total.

### IV. TDA linéaires classiques

#### A. Pile

Le TDA Pile_T manipule les types booléen et T et a comme opérations :
- creerPile : () ⟶ Pile_T
- estVide : pile_T ⟶ booléen
- empiler : Pile_T ✱ T ⟶ Pile_T
- depiler : pile_T ⟶ Pile_T
- tetePile : Pile_T ⟶ T

Axiomes :
- estVide(creerPile()) = Vrai
- estVide(empiler(P, x)) = Faux
- tetePile(empiler(P, x)) = x
- depiler(empiler(P, x)) = P

Implémentation : une pile sera un couple (tableau, entier)

Applications :
- traduire un programme en langage machine (utilisation d'automate à pile)
- exécution d'un programme (pour gérer les retours des appels de fonctions et les arguments des fonctions appelées)

#### B. File

Le TDA File_T utilise les types T et booléen et a comme opérations :
- creerFile : () ⟶ File_T
- fileVide : File_T ⟶ booléen
- enfiler : File_T ✱ T ⟶ File_T
- defiler : File_T ⟶ File_T
- teteFile : File_T ⟶ T

Axiomes :
- fileVide(creerFile()) = Vrai
- fileVide(enfiler(f, x)) = Faux
- fileVide(f) = Faux ⇒ teteFile(enfiler(f, x)) = teteFile(f)  
  ⇒ defiler(enfiler(f, x)) = enfiler(defiler(f), x)
- fileVide(f) = Vrai ⇒ teteFile(enfiler(f, x)) = x

Implémentations :
- Solution la moins couteuse en temps : on peut utiliser un tableau et 2 entiers qui représentent la tete de la file et la queue (on gère les indices modulo n pour un tableau circulaire)
- Solution à éviter : Déplacer le tableau vers la gauche lorsqu'on défile

Applications :
- Gestion de files d'attente
- Parcours de graphes
- Gestion de la mémoire ou des processus dans un système d'exploitation

#### C. Liste

On stocke un ensemble ordonné mais pas de façon contiguë contrairement aux tableaux. On accède à un élément à partir de son prédécesseur. L'accès à un élément quelconque d'une liste ne se fait pas en temps constant. L'avantage est qu'on peut insérer facilement dans une liste.

Une cellule_T est un enregistrement contenant une information à stocker de type T et une référence vers une cellule_T.

Le TDA Liste_T utilise les types booléen, T, entier et cellule_T et a comme opérations :
- valeurCellule : cellule_T ⟶ T
- suivant : cellule_T ⟶ cellule_T
- insererSuivant : cellule_T ✱ cellule_T ⟶ cellule_T
- derniereCellule : cellule_T ⟶ booléen
- creerListe : () ⟶ Liste_T
- estVideListe : Liste_T ⟶ booléen
- teteListe : Liste_T ⟶ cellule_T
- insererTete : Liste_T ✱ T ⟶ Liste_T
- supprimerTete : Liste_T ⟶ Liste_T
- taille : Liste_T ⟶ entier
- queue : Liste_T ⟶ Liste_T
- obtenirElement : Liste_T ✱ T ✱ entier ⟶ cellule_T
- insererElement : Liste_T ✱ entier ⟶ Liste_T

Implémentations :
- Tableaux :
  - Le suivant spatial est le suivant dans la liste. On a 2 entiers LS et LF, LS pointe sur le début de la liste et LF la fin, la liste est donc toujours un intervalle dans le tableau.
  - Gérer soi-meme les cases vides. Une cellule contient une information et un entier (-1 représente NULL) et le tableau sera un tableau de cellules. Pour gérer les cases vides on utilise une pile de cellules. Pour le faire en O(1), on considère la pile comme une liste.
- Allocation dynamique : La liste n'est pas gérée par un tableau, c'est une référence vers une cellule. Insérer un élément consiste a allouer de la mémoire pour une cellule qui sera insérer ensuite dans la liste.

Une *liste circulaire* est une liste où le dernier élément pointe sur le premier.  
Une *liste bi-directionnelle* est une liste où chaque élément de la liste référence le suivant et le précédent.


## Chapitre 3 : La complexité en temps

### I. Notations

Pour un problème donné, il peut exister plusieurs algorithmes corrects pour le résoudre. Quels sont les critères pour choisir le bon ?
- la lisibilité
- la facilité de l'implémentation dans un langage quelconque
- l'efficacité par rapport aux ressources disponibles (espace et temps)
On va se concentrer sur la complexité en temps, le nombre d'instructions de l'algorithme.

On va comparer le nombre d'instruction asymptotiquement. On utilise O pour borner supérieurement, Ω inférieurement et Θ pour encadrer.  
Une fonction peut être :
- constante O(1)
- linéaire O(n)
- polynomiale O(n^k)
- exponentielle O(n^n), O(n!), O(k^n)
- logarithmique O(log(n))

### II. Règles de calcul

Si I est une instruction alors C(I) est son temps d'exécution. Si A est un algorithme alors C(A)=ΣC(I) est son temps d'exécution.

Instructions atomiques de complexité O(1) :
- déclaration d'une variable
- affection à une variable
- lecture d'une variable
- opération arithmétique

Instructions composées :
- si I est x = exp, C(I) = C(exp)
- si I est f(e1, ..., en) alors C(I) = ΣC(ei)+C(f)
- si I est une boucle avec n itérations, à chaque passage on exécute li alors C(I) = ΣC(li)
- si I est une structure conditionnelle avec conditions booléennes alors C(I) est bornée supérieurement par max(ΣC(Cj)+C(li))

Taille des objets :  
- les nombres sont représentés en base 2
- la taille d'un ensemble est proportionnellement linéaire au nombre d'objets ✱ la taille d'un objet.

Un problème est dit *polynomial* s'il admet un algorithme qui s'exécute en temps polynomial sur la taille des entrées.  
Un problème est dit *non-déterministe polynomial* s'il existe un algorithme polynomial qui peut tester si une entrée est une solution au problème.

Analyser la complexité c'est expliqué pour quelles raisons on a la complexité "x" en expliquant instruction par instruction la complexité de chacune.

Pour simplifier, on considère que les opérations arithmétiques ont toujours une complexité en O(1). Pour un tableau, une pile, file ou liste de n objets de type T, sa taille est n ✱ la taille d'un objet de T. La complexité des opérations sur les piles et les files par représentation par tableau est O(1).

## Chapitre 4 : Types inductifs

### I. Rappels

**Définition :**  
Une définition inductive d'un ensemble E consiste a donner :
- un sous ensemble fini B de E
- un ensemble d'opérations K

>**Exemple :**  
B = {0}, K = {suc}, ar(suc) = 1 avec suc : n ↦ n + 1  
C'est la définition inductive de tous les entiers

En général c'est facile de montrer une propriété sur un ensemble inductif.
- On commence par montrer que c'est vrai sur les éléments de B.
- La partie inductive : pour chaque opération Φ∈K montrer que Φ(t1, ..., tk), k = ar(Φ) satisfait aussi la propriété.

C'est très intuitif d'imaginer un algorithme manipulant un ensemble inductif.
- La condition d'arrêt : éléments dans B
- La récursivité : par rapport aux opérations

### II. Arborescences

**Définition :**  
Une *arborescence*, notée (V, E, r), est un ensemble V muni d'une opération binaire E et d'un élément distingué r, défini inductivement par :
- tout singleton V est une arborescence
- si (V1, E1, r1), ..., (Vp, Ep, rp) sont des arborescences et r un objet alors (V, E, r) est une arborescence avec V = ∪Vi, E = (∪Ei)∪{(r, ri) | 1 ≤ i ≤ p}

**Terminologie :**  
- Les éléments de V sont appelés *noeuds*, r est appelé la *racine*, les éléments de E sont appelés *arcs*.  
- Pour un noeud *x*, l'ensemble {y | (x, y)∈E} est appelé l'ensemble des *fils de x*.  
- Pour tout noeud *x*, l'unique noeud Z tel que (Z, *x*)∈E est appelé *père de x*.  
- Un noeud sans fils est appelé *feuille*.
- Un chemin de taille k entre *x*1 et *x*k+1 est une séquence de noeuds (*x*1, ..., *x*i) tels que ∀ 1 ≤ i ≤ k, (*x*i, *x*i+1)∈E
- Si *x* est un noeud, on appelle *descendant de x*, l'ensemble des noeuds *y* tels qu'il existe un chemin de *x* à *y*. On le note Vˣ.

**Proposition :**  
Soit T = (V, E, r) une arborescence
1. pour tout noeud *x* il existe un unique chemin de r à *x*
2. T contient au moins une feuille
3. |E| = |V|-1
4. ht(T) = 1+max{ht(T^r) | Mi fils de r}

**Implémentation :**  
Ce qui va nous intéresser c'est :
- accéder à la racine
- pour un noeud donné, accéder à l'ensemble de ses fils
- pour un noeud donné, accéder à son père

Solution simple : si |V| = n, c'est associé (bijectivement) à chaque noeud un entier entre 0 et n-1. On va représenter l'arborescence par un tableau, nommé PERE, de taille n où ∀i, PERE[i] contient l'index dans le tableau du père du noeud associé à i. Pour la racine, si ir est son index alors PERE[ir] = -1.

Inconvénient : il faut connaitre à l'avance la taille du tableau.

Une deuxième solution : un noeud va s'auto-référencer. Une arborescence sera par exemple juste un noeud représentant la racine. 
