# Théorie des langages

## I. Langages

- **Langages**  
  Un *langage* est un ensemble de mots qui peut être défini :
  - Pour les langages finis :
    - En extension (liste de tous les mots du langage, ex : dictionnaire)
  - Pour les langages infinis :    
    - En compréhension (on commence une énumération, ex L={ab, aabb, aaabbb, ...})
    - En intension (on donne des règles)
    - Inductivement  


- **Définition inductive**
  - On donne une base d'objets appartenant à l'ensemble que l'ont veut définir
  - On donne des règles pour construire d’autres objets de l’ensemble à partir d’objets de la base ou d’objets déjà construits.

  > **Exemple :**  
  Définissons l'ensemble PAIR des entiers pairs positifs.  
  Base : 2 appartient à PAIR  
  Règle : Si x appartient à PAIR alors x+2 appartient à PAIR

  La définition inductive d'un ensemble n'est pas unique.

  > **Exemple :**  
  On peut aussi définir PAIR par:  
  Base : 2 appartient à PAIR  
  Règle : si x et y appartiennent à PAIR alors x+y appartient à PAIR


- **Fermeture inductive**   
  Soit U un univers et B⊂U une base, soit Ω une famille d’opérations sur U.  
  On appelle fermeture inductive de B par Ω la partie E de U définie par :
  - initialisation : B⊂E
  - construction : ∀f ∈ Ω et ∀x1 , x2 , ...xn∈E , si n est l’arité de f, si x=f(x1 , x2 , ...xn ) est défini, alors x∈E
  - fermeture : E est la plus petite partie de U qui contienne B et qui soit stable par Ω.

  > **Exemple :**  
  Liste


- **Langage**  
  Un *alphabet* Σ est un ensemble fini de caractères.  

  Un *mot* (appelé aussi une chaı̂ne) ω sur Σ est une suite finie de symboles de Σ juxtaposés.

  Sa *longueur* (= nombre de caractères) est notée |ω|. |ω| est le nombre d’occurrences de la lettre ’a’∈ Σ dans le mot ω.  

  Le *mot vide* ϵ ne contient aucun symbole. ϵ peut être un mot du langage mais n'est pas une lettre. |ϵ| = 0  

  Si α et β sont 2 mots sur Σ, on appelle *concaténation* de α et β le mot αβ. On peut concaténer avec ϵ. On notera a^n la concaténation de n occurences de a, a⁰ représente ϵ.  

  On appelle *facteur* ou sous-mot d’un mot ω un mot α tel qu’il existe 2 mots β et γ, avec ω = βαγ.  
  Si ω = α.β on dira que α est un préfixe de ω, et que β est un suffixe de ω.

  Soit Σ un alphabet, on appelle *fermeture de Kleene＊ de Σ, noté Σ＊ (l’ensemble des mots sur Σ, de longueur finie, plus le mot vide ϵ), l’ensemble défini inductivement de la façon suivante :
  - Base : tous les caractères de Σ ainsi que le mot vide ϵ sont dans Σ∗
  - Règle : si x et y sont dans Σ＊ , alors xy est dans Σ＊  

  On appelle *langage* sur un alphabet Σ, un sous ensemble de Σ∗.

  Soient L1 et L2 2 langages sur l’alphabet Σ  
  L1 ∪ L2 = {ω | ω ∈ L1 ou ω ∈ L2}  
  L1 ∩ L2 = {ω | ω ∈ L1 et ω ∈ L2}  

  Soient L1 un langage sur l’alphabet Σ1 et L2 un langage sur l’alphabet Σ2  
  L1.L2 = {ω1 ω2 ∈ (Σ1 ∪ Σ2)＊, ω1 ∈ L1 et ω2 ∈ L2}

  Fermeture de Kleene d’un langage :  
  L⁰ = {ϵ}  
  L^n = LL^(n−1) , ∀n≥1  
  L＊ = ∪ L^n , n≥0  
  Lᐩ = ∪ L^n , n>0

  Une *phrase ambigüe* est une phrase à laquelle on peut attribuer plusieurs sens.

  Une *interprétation* d’une phrase ambigüe est un sens que l’on attribue à cette phrase.

## II. Grammaire

**Backus-Naur Forme :**  
>**Exemple :**  
<phrase > : := <sujet ><verbe ><objet >  
<sujet > : := <déterminant ><nom >  
<objet > : := <déterminant ><nom >  
<verbe > : := mangent | voient  
<déterminant > : := les | des  
<nom > : := chats | lions | souris | jambons

![](img/arbre_grammaire_ex.png)

**Définition :**  
Une grammaire est un quadruplet G = (N, T, P, S) où :  
N est l’ensemble des symboles non terminaux (majuscule)  
T est l’ensemble des symboles terminaux, caractères de l’alphabet (minuscule)  
P est un ensemble de règles de production, de la forme α → β, avec α∈(N∪T)ᐩ, β∈(N∪T)＊. On peut avoir des règles de production dont la partie droite est réduite à ϵ, on appelera ces règles des ϵ-productions.  
S = symbole de départ appelé l’axiome
⟶ règle de production  
⇒ dérivation  
⇒ ＊ plusieurs dérivations

>**Exemple :**  
S → E  
E → E + E | (E) | 0E | 1E | 0 | 1  
N = {S, E}  
T = {0, 1, (, ), +}  
P = {S → E, E → E + E | (E) | 0E | 1E | 0 | 1}  

**Définition :**  
Pour une grammaire G , on note L(G) le langage engendré par G est l’ensemble des mots que l’on peut définir à partir de l’axiome de G en appliquant un nombre fini de fois des règles de G.  

>**Exemple :**  
On veut traduire le langage sur Σ = {a,b} où tous les mots sont de la forme ω = α aa β.  
S ⟶ AaaB  
A ⟶ aA | bA | ϵ  
B ⟶ aB | bB | ϵ

On peut factoriser la grammaire.

>**Exemple :**  
S → AaaB  
A → aA  
A → bA  
A →   
B → aB  
B → bB  
B → ϵ  
devient :  
S → AaaB  
A → aA|bA|ϵ  
B → aB|bB|ϵ  

**Définition :**  
Un arbre syntaxique est un arbre dont la racine est l’axiome (S), dont les noeuds internes sont étiquetés par des symboles de N, et dont les feuilles sont étiquetées par des symboles de T ou par le mot vide ϵ. Chaque noeud interne correspond à une règle de production.

**Définition :**  
On dit que deux grammaires G1 et G2 sont équivalentes, noté G1∼G2 , si elles engendrent le même langage, si L(G1) = L(G2).

**Définition :**  
On appelle dérivation gauche une suite de dérivations obtenues en choisissant à chaque étape le symbole non terminal le plus à gauche.  
On définit de façon similaire la dérivation droite.

**Définition :**  
On appelle langage engendré par une grammaire G = (N, T, P, S) l’ensemble des mots ω de T＊ tels que S ⇒＊ ω. On le note L(G). On dit qu’un mot ω est engendré par une grammaire G si ω∈L(G).

**Définition :**  
Une grammaire G est dite ambiguë s’il existe un mot ω de L(G) qui admet au moins deux arbres syntaxiques différents.  
Un langage est dit ambigü si toutes les grammaires qui l’engendrent sont ambiguës.

**Définition :**  
Une grammaire G est dite régulière si toutes ses règles de production sont de la forme :  
A → αB, avec : A∈N, α∈T∗, B∈N ou B = ϵ

>**Exemple :**  
S → aS|bS|a|b|ϵ

**Définition :**  
Un problème est dit indécidable si il n’existe pas (et il ne peut pas exister) d’algorithme générique pour le résoudre. S'il n'existe pas de grammaire régulière qui permet de le résoudre.

## III. Les langages rationnels

### A. Introduction

Les *langages rationnels* sont les plus simples, les plus rapides mais aussi les moins puissants. Ces langages sont carctérisés de plusieurs façons, ils sont :
- engendrés par une grammaire régulière
- décrits par une expression régulière
- engendrés par un automate d'état finis

**Théorème :**  
Un langage est rationnel ⇔ il existe une grammaire régulière qui l'engendre.

>**Exemple :**  
Langage des mots sur Σ = {a, b} qui contiennent le facteur aa:   
S ⟶ aS|bS|aA  
A ⟶ aB  
B ⟶ aB|bB|ϵ

### B. Les expressions régulières

Une expression régulière décrit un langage rationnel avec une syntaxe particulière et correspond à une grammaire régulière.

>**Exemple :**  
Sur Σ ={a, b}  
(a+b)＊ est l'ensemble des mots sur {a, b}

Une expression régulière est une expression algébrique qui permet de décrire un langage rationnel.

**Définition inductive:**  
Base : ∅ ,ϵ et les caractères de Σ sont des expressions régulières représentant respectivement les langages ∅, {ϵ}, {x} si x∈Σ  
Règles : si r et s sont des expressions régulières représentant les langages R et S alors (r+s), r.s, r＊ et rᐩ sont des expressions régulières représentant respectivement les langages R∪S, R.S, R＊ et Rᐩ.

On notera L(r) le langage rationnel décrit par l'expression r et on dira que r engendre le langage L(r).

**Remarques :**  
r+s se note aussi r|s  
a+b＊ s'interprète comme (a+(b＊))

**Propriétés :**  
(r＊)＊  
r(r＊) = (r＊)r = rᐩ  
(a＊b＊)＊ = (a+b)＊

Un meme langage rationnel peut etre décrit par plusieurs expressions régulières différentes.

Si L(r) = L(s), on dit que r et s sont des expressions régulières équivalentes noté r~s.

**Théorème :**  
Un langage est rationnel ⇔ il existe une expression régulière le reconnaissant.

### C. Les automates d'états finis

Un *système à états finis* est un modèle mathématique "discret". Il est composé d'un nombre fini de configurations, appelées des états, et d'actions permettant de passer d'un état à un autre. Les automates d'états finis sont des systèmes à états finis particuliers.

Un automate d'états finis est un graphe orienté fini dont les arcs sont étiquetés. Il est composé :
- d'un nombre fini de configurations (les états) qui sont les sommets du graphe
- d'actions permettant de passer d'un état à un autre qui sont les arcs du graphe
Il y a un seul état initial (noté par une flèche) et zéro, un ou plusieurs états finaux (double cerclage). Le parcours d'un graphe définit un mot du langage reconnu par l'automate.

>**Exemple :**  
![](img/automate1.png)

**Définition :**  
On peut étiqueter un arc d'un automate d'états finis par le mot vide ϵ (souvent noté par l'absence d'un caractère), il correspond à une ϵ-transition.  
On peut étiqueter une transition par plusieurs caractères : on en choisit un seul.  
On peut avoir des circuits, des boucles de réflexivité. On peut avoir plusieurs arcs sortants étiquetés par un même caractère.

>**Exemple :**
![](img/automate2.png)

Un *automate* d'états finis est donc défini par :
- un nombre fini d'états Q (les sommets du graphe)
- un alphabet Σ
- un ensemble fini ẟ de transitions (les arcs), étiquetés chacune par une ou plusieurs lettres de Σ ou par ϵ

Parmi les états de Q, on distingue :
- l'état initial q0∈Q
- les états finaux qui constituent l'ensemble F⊂Q

>**Exemple :**  
Quel est l'ensemble des solutions qui permettent au passeur d'emmener de la rive droite à la rive gauche le chou, la chèvre et le loup avec une barque ne pouvant en contenir qu'un sans laisser ensemble la chèvre et le loup ou la chèvre et le chou ?
![](img/automate3.png)

**Définition :**  
On dit qu'un automate d'états finis A = {Q, Σ, δ, q0, F} accepte un mot ω de Σ＊ ⇔ il existe au moins un chemin dans A allant de q0 à un état final, étiqueté par les lettres successives de ω, entre lesquelles on a éventuellement intercalé des occurrences de ϵ. Le langage L(A) reconnu par A est l'ensemble des mots que A accepte.

## IV. Équivalences d'automates

### A. Le problème du déterminisme

**Définition :**  
Un *automate* d'états finis déterministe est un automate d'états finis tel que, de chaque état q∈Q, il part |Σ| transitions, une pour chacune des lettres de l'alphabet Σ.

Dans un automate déterministe, on a un "état poubelle".

**Théorème :**  
La classe des langages reconnus par :
- les automates d'états finis déterministes
- les automates d'états finis non-déterministes sans ϵ-transition
- les automates d'états finis non-déterministes avec ϵ-transition
est la même : celle des langages rationnels.

**Exemple :**  
![](img/auto.png)

**Exemple :**  
![](img/auto2.png)

S | 0 | 1
:--: | :--: | :--:
Q0 = {q0} |
Q1 = {q0, q1} |
Q2 = {q0, q2} |
Q3 = {q0, q1, q3} |
Q4 = {q0, q3} |
Q5 = {q0, q1, q3} |
Q6 = {q0, q2, q3} |
Q7 = {q0, q1, q2, q3} |

![](img/auto3.png)
