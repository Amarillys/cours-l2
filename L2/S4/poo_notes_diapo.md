# Programmation orientée objets

## I. Introduction

- **Définition :**  
  Un *paradigme* est comment on va représenter les éléments du programme (données et traitements), les concepts.
  > **Exemple :**  
    Impératif (C, Pascal)  
    Fonctionnel (Lisp, Caml, Haskel)  
    Orienté objets (Java, C++)


- **Pourquoi autant de langages ?**

  - Pour des applications spécifiques
    > **Exemple :**  
      C (embarqué)  
      Perl (traitement de texte, scripts système)  
      PHP (web côté serveur)  
      Javascript (web côté client)  


- **Pourquoi différents paradigmes ?**

  - Pour différentes problématiques  
    > **Exemple :**  
      Fonctionnel : parallélisme, concurrent, distribué  
      Orienté objets : interface graphique
  - Langages multi-paradigmes  
  - Plateformes multi-langages


## II. Java

- **Pourquoi Java ?**

  - Libre et multi-plateformes   
  - Très utilisé  
  - Environnement riche  
  - Rigoureux  
  - Simple


- **Caractéristiques :**  

  - Très orienté objet
  - Basé sur les classes
  - Typage fort, statique, explicite
  - Héritage simple
  - Gestion de la mémoire automatique
  - Références sans pointeurs


- **Machine virtuelle :**  

  - Abstraction d'une machine réelle
  - Précompilation ⇒ Bytecode
  - Interprétation du bytecode


- **JIT :**

  - Plus rapide
  - Optimisation dynamique


![](img/compilation.png)  


## III. Concept d'objets

Un *objet* est une entité autonome qui contient des données qui sont masquées et un comportement qui va permettre de les manipuler. Un objet est donc responsable de son état et ne dépend que de lui. Cette isolation (encapsulation) apporte de la modularité.

La *modularité* est la décomposition en sous parties autonomes et indépendantes qui vont collaborer pour apporter du découplage.

![](img/modularite.png)

On peut voir une *classe* comme une définition de type, comme un ensemble d'objets qui ont les même Caractéristiques (d'un point de vu ensembliste), comme un modèle d'objet.

Une *instance* est un objet défini par la classe, une valeur du type de la classe.

Un *attribut* est une variable, donnée d'un objet qui est locale et qui défini l'état de l'objet. Un attribut peut être `public`, `private`, `protected`, `final`. Par défaut un attribut a une visibilité de paquet.


>**Exemple :**
```Java
class Personne {
  public String nom = "Coco";
  private final Date dateNaissance;
  protected Integer age;
  List<Personne> amis;
}
```
En UML :  
![](img/poo1.png)


Une *méthode* est une fonction qui permet de manipuler l'état d'un objet.

>**Exemple :**
```Java
class Forme {
  protected void dessiner() {
    //traitement dessin
  }
  private void deplacer(Point p) {
    //deplacement de la forme     
  }
  public Integer getSurface() {
    Integer s=0;
    //calcul de la surface
    return s;
  }
}
```
En UML :  
![](img/poo2.png)

On accède à un attribut ou à une méthode grace au point.

>**Exemple :**  
```Java
String sonNom = personne.nom;
forme.dessiner();
```

**Principe :** CQS (Command Query Separation)  
Une opération est :
- une requete sans effet de bord, retourne une valeur
- une commande avec effet de bord, retourne void

Un *constructeur* est une méthode particulière qui va initialiser l'instance. Le mot clé *new* permet de créer une nouvelle instance;

>**Exemple :**  
```Java
Personne p = new Personne();
Personne p = new Personne(new Date(1990, 4, 1), "Joe");
```

Le mot clé *this* permet d'accéder à l'instance elle-même.

Le *destructeur* est une méthode spéciale appelée à la suppression de l'objet pour libérer l'espace.

>**Exemple :**
Pour par exemple supprimer les fichiers temporaires.
```Java
public void finalize() {
  //destructeur
}
```

Un *accesseur* est une méthode qui donne accès aux attributs privés (getter/setter).
